module "network" {
  source          = "./network"
  token           = var.jr_yc_token
  cloud_id        = var.cloud_id
  folder_id       = var.folder_id
  zone            = "ru-central1-a"
  }

module "vm1" {
  name         = "vm1"
  source       = "./VM"
  token        = var.jr_yc_token
  cloud_id     = var.cloud_id
  folder_id    = var.folder_id
  zone         = "ru-central1-a"
  subnet_id    = module.network.subnet_id
  image_family = "ubuntu-2004-lts"
  ssh_key_pub  = var.ssh_key_pub
}

module "vm2" {
  name         = "vm2"
  source       = "./VM"
  token        = var.jr_yc_token
  cloud_id     = var.cloud_id
  folder_id    = var.folder_id
  zone         = "ru-central1-a"
  subnet_id    = module.network.subnet_id
  image_family = "ubuntu-2004-lts"
  ssh_key_pub  = var.ssh_key_pub
}

module "vm3" {
  name         = "vm3"
  source       = "./VM"
  token        = var.jr_yc_token
  cloud_id     = var.cloud_id
  folder_id    = var.folder_id
  zone         = "ru-central1-a"
  subnet_id    = module.network.subnet_id
  image_family = "centos-stream-8"
  ssh_key_pub  = var.ssh_key_pub
}
