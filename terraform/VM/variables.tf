variable "token" {
  description = "yc token"
  type        = string
  sensitive   = true
}

variable "cloud_id" {
  description = "yc cloud_id"
  type        = string
  sensitive   = false
}

variable "folder_id" {
  description = "yc folder_id"
  type        = string
  sensitive   = false
}

variable "name" {
  description = "instance name"
  type        = string
  sensitive   = false
}

variable "zone" {
  description = "yc zone"
  type        = string
  sensitive   = false
}

variable "subnet_id" {
  description = "subnet_id"
  type        = string
  sensitive   = false
}

variable "image_family" {
  description = "yc compute image family"
  type        = string
  default     = "lemp"
  sensitive   = false
}

variable "ssh_key_pub" {
  description = "ssh key public"
  type        = string
  sensitive   = false
}

variable "VM_cpu_count" {
  description = "VM_cpu_count"
  type        = string
  default     = "2"
  sensitive   = false
}

variable "VM_ram_size" {
  description = "VM_ram_size"
  type        = string
  default     = "2"
  sensitive   = false
}

variable "VM_disk_size" {
  description = "VM_disk_size"
  type        = string
  default     = "20"
  sensitive   = false
}

