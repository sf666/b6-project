data "yandex_compute_image" "image" {
  family = var.image_family
}

resource "yandex_compute_instance" "yc_ci" {
  name = "instance-${var.name}"

  resources {
    cores  = var.VM_cpu_count
    memory = var.VM_ram_size
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.image.id
      size = var.VM_disk_size
    }
  }

  network_interface {
    subnet_id = var.subnet_id
    nat       = true
  }

  metadata = {
    ssh-keys = "jr:${file(var.ssh_key_pub)}"
  }
}
