output "ip_address" {
  value = yandex_compute_instance.yc_ci.network_interface.0.ip_address
}

output "nat_ip_address" {
  value = yandex_compute_instance.yc_ci.network_interface.0.nat_ip_address
}
