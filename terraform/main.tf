terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "=0.84.0"
    }
  }
  required_version = ">= 0.13"

  backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "jr-tf-state"
    region     = "ru-central1"
    key        = "jr.tfstate"
    access_key = "YCAJEppC8yynAOGTy4HLCRAax"
    secret_key = "YCN9pNDuU_Mep653aIBFETqYda_CJRLkKnKIUfcj"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  token     = var.jr_yc_token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = var.zone
}

