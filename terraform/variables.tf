variable "jr_yc_token" {
  description = "yc token"
  type        = string
  sensitive   = true
}

variable "cloud_id" {
  description = "yc cloud_id"
  type        = string
  default     = "b1gn7pqebptv2d3411d8"
  sensitive   = false
}

variable "folder_id" {
  description = "yc folder_id"
  type        = string
  default     = "b1gl1ptki0e04btpmi18"
  sensitive   = false
}

variable "zone" {
  description = "yc zone"
  type        = string
  default     = "ru-central1-a"
  sensitive   = false
}

variable "ssh_key_pub" {
  description = "ssh key public"
  type        = string
  default     = "~/.ssh/id_rsa.pub"
  sensitive   = false
}

