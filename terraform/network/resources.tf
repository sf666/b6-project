resource "yandex_vpc_network" "network-1" {
  name = "network-lamp-lemp"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet-lamp-lemp"
  zone           = var.zone
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}
