 resource "local_file" "ansible_inventory" {
  content = templatefile("../ansible/templates/inventory.yaml",
    {
      vm1_ip_address = module.vm1.nat_ip_address
      vm1_ssh_user = "ubuntu"
      vm2_ip_address = module.vm2.nat_ip_address
      vm2_ssh_user = "ubuntu"
      vm3_ip_address = module.vm3.nat_ip_address
      vm3_ssh_user = "cloud-user"
    }
  )
  filename = "../ansible/inventory/b6.yaml"
}